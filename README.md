# mkinitcpio-ukify

Post hook for `mkinitcpio` to generate UKIs using `systemd-ukify`.

Optionally uses `ukify` configuration from:
- `/etc/initcpio/ukify/$preset-$p.conf`
- `/etc/initcpio/ukify/$preset.conf`
- `/etc/systemd/ukify.conf`

If the configuration doesn't provide `Cmdline` or `OSRelease`, those will be
generated automatically the same way `mkinitcpio` does.

If the configuration contains `Initrd`, those will be _prepended_ to the
`mkinitcpio` generated one. If it doesn't, existing `/boot/*-ucode.img` images
will be prepended instead.
